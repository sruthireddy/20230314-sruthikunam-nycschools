package com.nyc.schools.compose.ui.schools

import com.nyc.schools.compose.network.model.SchoolListApiModel
import com.nyc.schools.compose.repository.SchoolListRepository
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class SchoolListViewModelTest {
    @get:Rule
    val mainCoroutineRule = MainCoroutineRule()


    private val schoolListRepository: SchoolListRepository = mockk()

    private lateinit var viewModel: SchoolListViewModel
    private lateinit var dispatcherProvider: DispatcherProvider

    @OptIn(ExperimentalCoroutinesApi::class)
    @Before
    fun setup() {
        dispatcherProvider = TestDispatcherProvider()
        every { schoolListRepository.schoolLists } returns flow { emit(getSchoolListApiModel()) }
        viewModel = SchoolListViewModel(schoolListRepository)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun `Verify SchoolListApiModel`() = runTest {
        delay(2000)
        assertEquals(false, viewModel.uiState.isLoading)
        assertEquals(false, viewModel.uiState.offline)
        assertEquals(1, viewModel.uiState.list.size)
        assertEquals("dbn", viewModel.uiState.list[0].dbn)
        assertEquals("numOfSatTestTakers", viewModel.uiState.list[0].numOfSatTestTakers)
        assertEquals(
            "satCriticalReadingAvgScore",
            viewModel.uiState.list[0].satCriticalReadingAvgScore
        )
        assertEquals("satMathAvgScore", viewModel.uiState.list[0].satMathAvgScore)
        assertEquals("satWritingAvgScore", viewModel.uiState.list[0].satWritingAvgScore)
        assertEquals("schoolName", viewModel.uiState.list[0].schoolName)
    }


    private fun getSchoolListApiModel(): List<SchoolListApiModel> = listOf(
        SchoolListApiModel(
            dbn = "dbn",
            numOfSatTestTakers = "numOfSatTestTakers",
            satCriticalReadingAvgScore = "satCriticalReadingAvgScore",
            satMathAvgScore = "satMathAvgScore",
            satWritingAvgScore = "satWritingAvgScore",
            schoolName = "schoolName"
        )
    )
}