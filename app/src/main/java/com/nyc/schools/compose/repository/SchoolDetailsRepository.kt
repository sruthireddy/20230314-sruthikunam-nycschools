package com.nyc.schools.compose.repository

import com.nyc.schools.compose.network.SchoolDetailsApi
import com.nyc.schools.compose.network.model.SchoolDetailsApiModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class SchoolDetailsRepository @Inject constructor(
    private val schoolDetailsApi: SchoolDetailsApi,
) {
    fun getUserDetails(dbn: String): Flow<SchoolDetailsApiModel?> = flow {
        val filter = schoolDetailsApi.getSchoolDetails().filter { it.dbn == dbn }
        if (filter.isNotEmpty()) {
            emit(filter.first())
        } else {
            emit(SchoolDetailsApiModel(empty=true))
        }
    }
}