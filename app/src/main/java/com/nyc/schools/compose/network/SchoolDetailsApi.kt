package com.nyc.schools.compose.network

import com.nyc.schools.compose.network.model.SchoolDetailsApiModel
import retrofit2.http.GET

interface SchoolDetailsApi {

    @GET("resource/s3k6-pzi2.json")
    suspend fun getSchoolDetails(): List<SchoolDetailsApiModel>
}