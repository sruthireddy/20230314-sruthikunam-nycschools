package com.nyc.schools.compose.network

import com.nyc.schools.compose.network.model.SchoolListApiModel
import retrofit2.http.GET

interface SchoolListApi {

    @GET("resource/f9bf-2cp4.json")
    suspend fun getSchoolList(): List<SchoolListApiModel>
}