package com.nyc.schools.compose.ui.schools

import android.annotation.SuppressLint
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.School
import androidx.compose.material.icons.sharp.ChevronRight
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Color.Companion.Black
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.nyc.schools.compose.commoncomposeview.LoadingView
import com.nyc.schools.compose.network.model.SchoolListApiModel
import com.nyc.schools.compose.ui.components.NoNetwork


@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SchoolListMainScreen(
    onUserClick: (String, String) -> Unit
) {
    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Text("NYC Schools")
                },
                colors = TopAppBarDefaults.mediumTopAppBarColors(
                    containerColor = MaterialTheme.colorScheme.primary.copy(alpha = 0.95f),
                    titleContentColor = Color.White
                ),
            )
        },
        content = { SchoolListScreen(onUserClick) },
    )
}

@Composable
fun SchoolListScreen(
    onUserClick: (String, String) -> Unit
) {
    val viewModel = hiltViewModel<SchoolListViewModel>()
    val uiState = viewModel.uiState
    if (uiState.offline) {
        NoNetwork()
    } else if (uiState.isLoading) {
        LoadingView()
    } else {
        LazyColumn(
            modifier = Modifier
                .fillMaxSize()
                .offset(y = (65).dp)
                .background(MaterialTheme.colorScheme.background)
        ) {
            items(uiState.list) { item ->
                SchoolItem(item = item, onUserClick = onUserClick)
                Divider(color = Black, thickness = 1.dp)
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SchoolItem(item: SchoolListApiModel, onUserClick: (String, String) -> Unit) {
    ListItem(modifier = Modifier.clickable { onUserClick(item.dbn, item.schoolName) },
        headlineText = {
            Text(
                text = item.schoolName,
                modifier = Modifier
                    .fillMaxWidth()
                    .wrapContentWidth(Alignment.Start)
            )
        },
        overlineText = {
            Text(
                text = item.dbn,
                modifier = Modifier
                    .fillMaxWidth()
                    .wrapContentWidth(Alignment.Start)
            )
        },
        leadingContent = {
            Image(
                imageVector = Icons.Default.School,
                contentDescription = null,
                modifier = Modifier
                    .size(40.dp)
                    .clip(CircleShape)

            )
        },
        trailingContent = {
            Image(
                imageVector = Icons.Sharp.ChevronRight,
                contentDescription = null,
                modifier = Modifier
                    .size(40.dp)
                    .clip(CircleShape)
            )
        }
    )
}

