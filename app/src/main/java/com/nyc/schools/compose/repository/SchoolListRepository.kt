package com.nyc.schools.compose.repository

import com.nyc.schools.compose.network.SchoolListApi
import com.nyc.schools.compose.network.model.SchoolListApiModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

open class SchoolListRepository @Inject constructor(
    private val schoolListApi: SchoolListApi,
) {

    val schoolLists: Flow<List<SchoolListApiModel>?> = flow {
        emit(schoolListApi.getSchoolList())
    }.flowOn(Dispatchers.Main)
}