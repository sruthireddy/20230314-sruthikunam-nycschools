package com.nyc.schools.compose.ui.schooldetails

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import com.nyc.schools.compose.commoncomposeview.ErrorText
import com.nyc.schools.compose.ui.components.NoNetwork
import com.nyc.schools.compose.commoncomposeview.LoadingView
import com.nyc.schools.compose.ui.Argument

@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SchoolDetailsMainScreen(navController: NavHostController) {
    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Text(
                        navController.currentBackStackEntry?.arguments?.getString(Argument.SCHOOL_NAME)
                            ?: "NYC School",
                        maxLines = 1
                    )
                },
                colors = TopAppBarDefaults.mediumTopAppBarColors(
                    containerColor = MaterialTheme.colorScheme.primary,
                    titleContentColor = Color.White
                ),
                navigationIcon = {
                    IconButton(onClick = { navController.navigateUp() }) {
                        Icon(
                            imageVector = Icons.Filled.ArrowBack,
                            tint = Color.White,
                            contentDescription = "Back"
                        )
                    }
                },
            )
        },
        content = { SchoolDetailsScreen() },
    )
}

@Composable
fun SchoolDetailsScreen() {
    val viewModel = hiltViewModel<SchoolDetailsViewModel>()
    val uiState = viewModel.schoolDetailsUiState
    val ctx = LocalContext.current

    if (uiState.offline) {
        NoNetwork()
    } else if (uiState.isLoading) {
        LoadingView()
    } else if (uiState.isEmptyResponse) {
        ErrorText(text = "Data not Found")
    } else {
        Card(
            modifier = Modifier
                .fillMaxWidth()
                .offset(y = (65).dp)
                .padding(15.dp)
                .clickable { },
            elevation = CardDefaults.cardElevation(defaultElevation = 20.dp)
        ) {
            Column(
                modifier = Modifier.padding(15.dp)
            ) {
                Column {
                    Text(
                        modifier = Modifier.padding(start = 25.dp),
                        text = uiState.detail.schoolName ?: "",
                        fontSize = 25.sp,
                        color = MaterialTheme.colorScheme.primary
                    )
                    Text(
                        modifier = Modifier.padding(start = 16.dp, top = 16.dp),
                        text = buildAnnotatedString {
                            withStyle(
                                style = SpanStyle(
                                    fontWeight = FontWeight.W900,
                                    color = Color.DarkGray
                                )
                            ) {
                                append("Primary Address::")
                            }
                            append(uiState.detail.primaryAddressLine1.orEmpty())

                        },
                        color = MaterialTheme.colorScheme.onBackground
                    )
                    Text(
                        modifier = Modifier.padding(start = 16.dp, top = 5.dp),
                        text = buildAnnotatedString {
                            withStyle(
                                style = SpanStyle(
                                    fontWeight = FontWeight.W900,
                                    color = Color.DarkGray
                                )
                            ) {
                                append("Zip Code::")
                            }
                            append(uiState.detail.zip.orEmpty())
                        },
                        color = MaterialTheme.colorScheme.onBackground
                    )

                    Text(
                        modifier = Modifier
                            .padding(start = 16.dp, top = 5.dp)
                            .clickable {
                                val u = Uri.parse("tel:" + uiState.detail.phoneNumber)
                                val i = Intent(Intent.ACTION_DIAL, u)
                                try {
                                    ctx.startActivity(i)
                                } catch (s: SecurityException) {
                                }

                            },
                        text = buildAnnotatedString {
                            withStyle(
                                style = SpanStyle(
                                    fontWeight = FontWeight.W900,
                                    color = Color.DarkGray
                                )
                            ) {
                                append("Phone Number::::")
                            }
                            append(uiState.detail.phoneNumber.orEmpty())
                        },
                        color = MaterialTheme.colorScheme.onBackground
                    )

                    Text(
                        modifier = Modifier.padding(start = 16.dp, top = 5.dp),
                        text = buildAnnotatedString {
                            withStyle(
                                style = SpanStyle(
                                    fontWeight = FontWeight.W900,
                                    color = Color.DarkGray
                                )
                            ) {
                                append("Fax Number::::")
                            }
                            append(uiState.detail.faxNumber.orEmpty())
                        },
                        color = MaterialTheme.colorScheme.onBackground
                    )
                    Text(
                        modifier = Modifier.padding(start = 16.dp, top = 5.dp).clickable {
                            val intent = Intent(Intent.ACTION_SENDTO).apply {
                                data = Uri.parse("mailto:")
                                putExtra(Intent.EXTRA_EMAIL, uiState.detail.schoolEmail)
                            }
                            try {
                                ctx.startActivity(intent)
                            } catch (s: SecurityException) {
                            }
                        },
                        text = buildAnnotatedString {
                            withStyle(
                                style = SpanStyle(
                                    fontWeight = FontWeight.W900,
                                    color = Color.DarkGray
                                )
                            ) {
                                append("School Email::::")
                            }
                            append(uiState.detail.schoolEmail.orEmpty())
                        },
                        color = MaterialTheme.colorScheme.onBackground
                    )
                    Text(
                        modifier = Modifier.padding(start = 16.dp, top = 5.dp),
                        text = buildAnnotatedString {
                            withStyle(
                                style = SpanStyle(
                                    fontWeight = FontWeight.W900,
                                    color = Color.DarkGray
                                )
                            ) {
                                append("Sports::::")
                            }
                            append(uiState.detail.schoolSports.orEmpty())
                        },
                        color = MaterialTheme.colorScheme.onBackground
                    )
                    Text(
                        modifier = Modifier.padding(start = 16.dp, top = 5.dp),
                        text = buildAnnotatedString {
                            withStyle(
                                style = SpanStyle(
                                    fontWeight = FontWeight.W900,
                                    color = Color.DarkGray
                                )
                            ) {
                                append("City::::")
                            }
                            append(uiState.detail.city.orEmpty())
                        },
                        color = MaterialTheme.colorScheme.onBackground
                    )
                    Text(
                        modifier = Modifier.padding(start = 16.dp, top = 16.dp),
                        text = buildAnnotatedString {
                            withStyle(
                                style = SpanStyle(
                                    fontWeight = FontWeight.W900,
                                    color = Color.DarkGray
                                )
                            ) {
                                append("Overview::::")
                            }
                        },
                        color = MaterialTheme.colorScheme.onBackground
                    )
                    Text(
                        modifier = Modifier.padding(start = 16.dp, top = 5.dp),
                        text = uiState.detail.overviewParagraph.orEmpty(),
                        color = MaterialTheme.colorScheme.onBackground
                    )
                }
            }
        }
    }
}

