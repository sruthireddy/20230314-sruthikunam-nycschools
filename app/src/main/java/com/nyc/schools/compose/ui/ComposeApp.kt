package com.nyc.schools.compose.ui

import androidx.compose.runtime.Composable
import androidx.lifecycle.Lifecycle
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.nyc.schools.compose.ui.schooldetails.SchoolDetailsMainScreen
import com.nyc.schools.compose.ui.schools.SchoolListMainScreen

@Composable
fun ComposeApp() {
    val navController = rememberNavController()
    NavHost(
        navController = navController,
        startDestination = Route.DBN_ROUTE
    ) {
        composable(Route.DBN_ROUTE) { backStackEntry ->
            SchoolListMainScreen(
                onUserClick = { dbnId,schoolName ->
                    // In order to discard duplicated navigation events, we check the Lifecycle
                    if (backStackEntry.lifecycle.currentState == Lifecycle.State.RESUMED) {
                        navController.navigate("${Route.DETAIL_ROUTE}/$dbnId/$schoolName")
                    }
                }
            )
        }
        composable(
            route = "${Route.DETAIL_ROUTE}/{${Argument.DBN_ID}}/{${Argument.SCHOOL_NAME}}",
            arguments = listOf(
                navArgument(Argument.DBN_ID) {
                    type = NavType.StringType
                },
                navArgument(Argument.SCHOOL_NAME) {
                    type = NavType.StringType
                }
            ),
        ) {
            SchoolDetailsMainScreen(navController)
        }
    }
}

object Route {
    const val DBN_ROUTE = "school_list"
    const val DETAIL_ROUTE = "school_detail"
}

object Argument {
    const val DBN_ID = "dbn_id"
    const val SCHOOL_NAME = "school_name"
}