package com.nyc.schools.compose.ui.schools

import android.util.Log
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.nyc.schools.compose.repository.SchoolListRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class SchoolListViewModel @Inject constructor(
    private val schoolListRepository: SchoolListRepository
) : ViewModel() {

    var uiState by mutableStateOf(SchoolItemUiState())
        private set

    init {
        retrieveSchoolListApiModel()
    }

    private fun retrieveSchoolListApiModel() {
        viewModelScope.launch(Dispatchers.IO) {
            schoolListRepository.schoolLists.collect { list ->
                uiState = if (list.isNullOrEmpty()) {
                    uiState.copy(offline = true, isLoading = false)
                } else {
                    uiState.copy(
                        list = list,
                        offline = false,
                        isLoading = false
                    )
                }
            }
        }

    }

}