package com.nyc.schools.compose.ui.schooldetails

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.nyc.schools.compose.repository.SchoolDetailsRepository
import com.nyc.schools.compose.ui.Argument
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class SchoolDetailsViewModel @Inject constructor(
    private val detailsRepository: SchoolDetailsRepository,
    savedStateHandle: SavedStateHandle,
) : ViewModel() {

    private val dbnId: String? = savedStateHandle[Argument.DBN_ID]
    var schoolDetailsUiState by mutableStateOf(SchoolDetailsUiState())
        private set

    init {
        dbnId?.let {
            viewModelScope.launch(Dispatchers.IO) {
                try {
                    detailsRepository.getUserDetails(it).collect { detail ->
                        withContext(Dispatchers.Main) {
                            schoolDetailsUiState = if (detail == null) {
                                schoolDetailsUiState.copy(offline = true, isLoading = false)
                            } else if (detail.empty) {
                                schoolDetailsUiState.copy(
                                    offline = true, isLoading = false, isEmptyResponse = true
                                )
                            } else {
                                schoolDetailsUiState.copy(
                                    detail = detail, offline = false, isLoading = false
                                )
                            }
                        }
                    }
                } catch (e: Exception) {
                    schoolDetailsUiState.copy(
                        offline = true, isLoading = false, isEmptyResponse = true
                    )
                }
            }
        }
    }
}