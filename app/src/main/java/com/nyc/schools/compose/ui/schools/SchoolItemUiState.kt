package com.nyc.schools.compose.ui.schools

import com.nyc.schools.compose.network.model.SchoolListApiModel

data class SchoolItemUiState(
    val list: List<SchoolListApiModel> = listOf(),
    val offline: Boolean = false,
    val isLoading:Boolean = true
)