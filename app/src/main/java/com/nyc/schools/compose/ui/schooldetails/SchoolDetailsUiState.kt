package com.nyc.schools.compose.ui.schooldetails

import com.nyc.schools.compose.network.model.SchoolDetailsApiModel

data class SchoolDetailsUiState(
    val detail: SchoolDetailsApiModel = SchoolDetailsApiModel(),
    val offline: Boolean = false,
    val isLoading: Boolean = true,
    val isEmptyResponse:Boolean = false
)