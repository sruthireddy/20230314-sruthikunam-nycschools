package com.nyc.schools.compose.commoncomposeview

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.nyc.schools.R

@Composable
fun LoadingView() {
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = CenterHorizontally
    ) {
        CircularProgressIndicator(modifier = Modifier.wrapContentWidth(CenterHorizontally))
    }
}

@Suppress("unused")
@Composable
fun rowSpacer(value: Int) = Spacer(modifier = Modifier.height(value.dp))

@Composable
fun columnSpacer(value: Int) = Spacer(modifier = Modifier.height(value.dp))

@Composable
fun ErrorView(message: String = "Oops! Something went wrong,\n Please refresh after some time!") {
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = CenterHorizontally
    ) {
        Image(
            modifier = Modifier
                .size(100.dp)
                .padding(bottom = 16.dp),
            painter = painterResource(R.drawable.ic_offline),
            colorFilter = ColorFilter.tint(MaterialTheme.colorScheme.error),
            contentDescription = null
        )

        columnSpacer(value = 12)
        ErrorText(message)
    }
}


@Composable
fun EmptyView(message: String = "Nothing in here Yet!, Please wait...") {
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = CenterHorizontally
    ) {
        Image(
            modifier = Modifier
                .size(100.dp)
                .padding(bottom = 16.dp),
            painter = painterResource(R.drawable.ic_offline),
            colorFilter = ColorFilter.tint(MaterialTheme.colorScheme.error),
            contentDescription = null
        )
        columnSpacer(value = 12)
        EmptyText(message)
    }
}

@Composable
fun ErrorText(text: String, modifier: Modifier = Modifier) {
    Text(
        text = text, modifier = modifier
            .fillMaxWidth()
            .padding(horizontal = 48.dp),
        style = MaterialTheme.typography.bodyMedium,
        textAlign = TextAlign.Center, color = MaterialTheme.colorScheme.error.copy(alpha = 0.9F)
    )
}

@Composable
fun EmptyText(text: String, modifier: Modifier = Modifier) {
    Text(
        text = text, modifier = modifier
            .fillMaxWidth()
            .padding(horizontal = 48.dp),
        style = MaterialTheme.typography.bodyMedium,
        lineHeight = 20.sp,
        textAlign = TextAlign.Center
    )
}
