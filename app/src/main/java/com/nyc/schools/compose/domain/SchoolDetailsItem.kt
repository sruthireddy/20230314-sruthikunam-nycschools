package com.nyc.schools.compose.domain

data class SchoolDetailsItem(
    val user: String? = "",
    val avatar: String? = "",
    val name: String? = "",
    val userSince: String? = "",
    val location: String? = ""
)